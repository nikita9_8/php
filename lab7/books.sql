-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 25 2018 г., 11:51
-- Версия сервера: 5.6.38
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `books`
--

-- --------------------------------------------------------

--
-- Структура таблицы `authors`
--

CREATE TABLE `authors` (
  `id` int(100) NOT NULL,
  `lastname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `authors`
--

INSERT INTO `authors` (`id`, `lastname`) VALUES
(1, 'Кинг'),
(2, 'Сапковский'),
(3, 'Дюма'),
(4, 'Кристи'),
(5, 'Гриллс');

-- --------------------------------------------------------

--
-- Структура таблицы `basket`
--

CREATE TABLE `basket` (
  `login` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `quantity` int(20) NOT NULL,
  `price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `basket`
--

INSERT INTO `basket` (`login`, `name`, `quantity`, `price`) VALUES
('Nik_ch', 'Никита', 5, 4000),
('Danich', 'Данил', 2, 2000),
('Никита', 'Ведьмак', 1, 800),
('Андрей', 'Дюма', 3, 3000),
('Никита', 'Серия Метро', 3, 4000),
('Никита', 'Кинг Туман', 1, 400),
('Андрей', 'Метро', 1, 800),
('Андрей', 'Дьявольное святилище', 1, 500),
('Никита', 'Сад Костей', 1, 200);

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `id` int(100) NOT NULL,
  `author_id` int(100) NOT NULL,
  `press_id` int(100) NOT NULL,
  `category_id` int(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  `year` date NOT NULL,
  `picture` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `pages` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `author_id`, `press_id`, `category_id`, `name`, `year`, `picture`, `description`, `price`, `pages`) VALUES
(1, 1, 3, 4, 'Мобильник', '2013-11-04', 'Красивая', 'Ужасы про зомби', 800, 450),
(2, 4, 1, 2, 'Убийство за обедом', '2012-03-19', 'Кровь и сигареты', 'Расследования убийства за обедом', 400, 500),
(3, 3, 2, 3, 'Три мушкетера', '2013-10-15', 'Мушкеты', 'Приключение мушкетеров', 1000, 400),
(4, 2, 3, 1, 'Ведьмак Сага', '2012-03-19', 'Геральд', 'Монстры', 800, 1500),
(5, 1, 2, 4, 'Туман', '2018-10-08', 'Туманный город', 'Потусторонний мир', 300, 200);

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(100) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Фанатастика'),
(2, 'Детектив'),
(3, 'Романы'),
(4, 'Ужасы');

-- --------------------------------------------------------

--
-- Структура таблицы `name`
--

CREATE TABLE `name` (
  `name` varchar(20) NOT NULL,
  `picture` varchar(20) NOT NULL,
  `description` varchar(20) NOT NULL,
  `price` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `name`
--

INSERT INTO `name` (`name`, `picture`, `description`, `price`) VALUES
('Сапковский', 'Геральд', 'Ведьмак', 800),
('Кинг', 'Телефон', 'Мобильник', 600),
('Дюма', 'мушкет', 'Три мушкетера', 500),
('Дюма', 'Тюльпан', 'Черный тюльпан', 500);

-- --------------------------------------------------------

--
-- Структура таблицы `presses`
--

CREATE TABLE `presses` (
  `id` int(100) NOT NULL,
  `namep` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `presses`
--

INSERT INTO `presses` (`id`, `namep`, `address`) VALUES
(1, '«Книжный Клуб «Клуб Семейного Досуга»', 'Киев'),
(2, 'Клуб любителей книг', 'Львов'),
(3, 'Фонтастические кНиги', 'Запорожье');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`),
  ADD KEY `press_id` (`press_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `presses`
--
ALTER TABLE `presses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `authors`
--
ALTER TABLE `authors`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `presses`
--
ALTER TABLE `presses`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`),
  ADD CONSTRAINT `books_ibfk_2` FOREIGN KEY (`press_id`) REFERENCES `presses` (`id`),
  ADD CONSTRAINT `books_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
