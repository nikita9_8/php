<?php

    $dblocation = "localhost";
    $database = "books";
    $dbuser = "root";
    $dbpassword = "";
    $mysqli = new mysqli($dblocation, $dbuser, $dbpassword, $database);
    if ($mysqli->connect_errno) {
        echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }


    $query = 'SELECT * FROM `name` WHERE price > 500';
    $res = $mysqli->query($query);
    if ($res === false) {
        throw new \Exception('Ошибка в SQL-запросе!');
    }
    $entries = $res->fetch_all(MYSQLI_ASSOC);


    $query = 'SELECT * FROM `basket` WHERE login = \'Никита\' ';
    $res = $mysqli->query($query);
    if ($res === false) {
        throw new \Exception('Ошибка в SQL-запросе!');
    }
    $entries2 = $res->fetch_all(MYSQLI_ASSOC);

    $query = 'select a.lastname,b.name,p.address,p.namep,b.year,b.pages
                from books b
                join authors a on a.id = b.author_id
                join presses p on p.id = b.press_id
                join categories c on c.id = b.category_id
                where b.year < \'2014-01-01\'';
    $res = $mysqli->query($query);
    if ($res === false) {
        throw new \Exception('Ошибка в SQL-запросе!');
    }
    $entries3 = $res->fetch_all(MYSQLI_ASSOC);
    var_dump($entries3);


    mysqli_close($mysqli);



?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>FourLab</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

        <h1>Первое Задание</h1>
        <br>
        <br>
        <?

          foreach ($entries as $key) {
              echo "<h1>" . $key['name'] . ' - ' . $key['description'] . ' - ' . $key['price'] . "</h1>";
          }

        ?>
        <hr>
        <h1>Второе Задание</h1>
        <br>
        <br>
        <?

        $name = "";
        $coust = 0;
        $count = 0;

        foreach ($entries2 as $key) {
            echo "<h2>" . $key['login'] . ' - ' . $key['name'] . ' - ' . $key['quantity'] . ' - ' . $key['price'] . "</h2>";
            $coust += $key['price'];
            $count += $key['quantity'];
            $name = $key['login'];
        }
        echo "<h1>" . "Сумарный заказ" . "</h1>";
        echo "<h2>" . $name . " заказал " . $count . " книг на сумму " . $coust . " Гривен" . "</h2>";


        ?>
        <hr>
        <h1>Третье Задание</h1>
        <br>
        <br>
        <?php

        foreach ($entries3 as $key) {
            echo "<h2>" . $key['lastname'] . '. ' . $key['name'] . ' - ' . $key['address'] . ': ' . $key['namep'] . ', ' . $key['year'] . '. - ' . $key['pages'] . "</h2>";
        }

        ?>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="js/script.js"></script>
</body>
</html>