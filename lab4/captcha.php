<?php
    session_start();

    $random = rand(1000,9999);

    $_SESSION['rand'] = $random;

    $im = @imagecreatetruecolor(100, 38);
    $white = imagecolorallocate($im,255,255,255);
    $black = imagecolorallocate($im,0,0,0);
    imagefilledrectangle($im, 0,0,0,0,$black);
    $font = dirname(__FILE___).'/fonts/OpenSans-Regular.ttf';
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");//защита от кеширования
    header("Last-Modified: " . gmdate("D, d M Y H:i:s", 10000) . " GMT");//защита от кеширования
    header("Cache-Control: no-store, no-cache, must-revalidate");//защита от кеширования
    header("Cache-Control: post-check=0, pre-check=0", false);//защита от кеширования
    header("Pragma: no-cache");//защита от кеширования

    header("Content-Type:image/png");    //тип контента

    imagettftext($im,25,5,15,30,$white,$font,$random);
    imagegif($im);
    imagedestroy($im);

?>