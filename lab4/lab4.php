<?php

    // Lab4

    session_start();

    if ($_SESSION['rand']  == $_POST['code']) {

        echo "
                
                    <script>
                    
                        alert(\" Каптча верна \");
                    
                    </script>
                
                ";

    } else {
        echo "
                
                    <script>
                        
                        alert(\" Ошибка ввода капчи \");
                        window.location = \"index.php\";
                    
                    </script>
                
                ";
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RequestPage</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<section class="mainSection container">
    <main class="mainSection-form">
        <?php
        /**
         * Created by PhpStorm.
         * User: mikit
         * Date: 06.09.2018
         * Time: 15:25
         */

        $str = "Добрий день,"
            .$_POST["first_name"]. "  "
            .$_POST["last_name"]."!";
        echo "<h1 class=\"display-4 text-center\">{$str}</h1>";
        $arr = array();

        foreach ($_POST["kurs"] as $key => $value ) {
            $arr[$key] .= $value;
        }
        $strArr = implode(", ", $arr);

        echo "<h2 class=\"display-4 text-center\"> Ви вибрали для вивчення курс по {$strArr} </h2>";

        // lab 4 part1
        $date1 = trim($_POST['trip']);
        $date2 = date('m-d-Y h:i:s a', time());

        $diff = abs(strtotime($date2) - strtotime($date1));

        $years   = floor($diff / (365*60*60*24));
        $months  = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days    = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        $hours   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
        $minuts  = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
        $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));

        echo "<p style='text-align: center; font-size: 25px;'> Ваш вік - ", "Років:",$years, ' ', "Місяців:",$months, ' ', "Днів:",$days, ' ', "Годин:",$hours, ' ', "Хвилин:",$minuts, ' ', "Секунд:",$seconds . "</p>";

        // end lab 4 part1
        ?>

        <button type="button" class="btn btn-outline-primary logs-btn" data-toggle="modal" data-target="#exampleModal">
            Посмотреть логи посетителей
        </button>

        <?php

        //Лаб 2
        function arrToString($arr) {
            return implode("#", $arr);
        }

        $file = fopen('data.log', 'a+') or die ("Непредвиденная ошибка");

        $personArr = array();

        //Занесение данных в отдельный массив из Post запроса
        foreach ($_POST as $key => $item) {
            $personArr[$key] .= $item;
        }

        //Из массива в строку
        $person = arrToString($personArr);
        //запись в файл
        fwrite($file, $person . PHP_EOL);

        //переводим данные файла в массив
        $fileArr = file('data.log');
        $arrStrFromFile = array();
        //делаем стек из массива логов
        foreach ($fileArr as $item) {
           array_push($arrStrFromFile,explode("#",$item));
        }
        /*
         * Данный блок отвечает за выборку IP адрессов из стека логов файла
         * */
        $arrIP = array();

        foreach ($arrStrFromFile as $item) {
            array_push($arrIP , $item[1]);
        }

        //print_r($arrIP);

        fclose($file);

        //Записываем данные массива с выборкой в .dat файл, прежде сортируем его по алфавиту
        sort($arrIP);
        $arrIP = implode("\n", $arrIP);

        $logFile = fopen('login.dat', 'w+') or die ("Непредвиденная ошибка");

        fwrite($logFile,$arrIP);

        fclose($logFile);
        ?>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Логи </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php

                        $arrFromLog = file('login.dat');
                        $outerData = array();

                        foreach ($arrFromLog as $item) {
                            array_push($outerData,$item);
                        }

                        $strLogs = implode("<br>", $outerData);

                        echo "<p class=\"logs text-center\">{$strLogs} </p>";


                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>

    </main>
</section>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
