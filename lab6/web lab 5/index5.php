﻿<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        table {
            max-width: 100%;
        }
    </style>
</head>
<body>
<?php
$html = file_get_contents("https://www.wikipedia.org/");

preg_match_all("/<[a][\s]{1}[^>]*[Hh][Rr][Ee][Ff][^=]*=[ '\"\s]*([^ \"'>\s#]+)[^>]*>/", $html, $url);
$urls = $url[1];
echo "<table class=\"table\" border='1'>";
    echo "<thead>";
	echo "<tr>";
	echo "<th scope=\"col\" colspan='2'><a href='https://www.wikipedia.org/'>исходник</a></th>";
	echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
for ($i = 0; $i < count($urls); $i++) {
	echo "<tr>";
	echo "<td>Посилання</td>";
	echo "<td>".$urls[$i]."</td>";
	echo "</tr>";
}

preg_match_all('~[-a-z0-9_]+(?:\\.[-a-z0-9_]+)*@[-a-z0-9]+(?:\\.[-a-z0-9]+)*\\.[a-z]+~i', $html, $email);
$emails = $email[0];
for ($i = 0; $i < count($emails); $i++) {
	echo "<tr>";
	echo "<td>Email</td>";
	echo "<td>".$emails[$i]."</td>";
	echo "</tr>";
}

preg_match_all("/<h+[0-9]>(.*?)<\/h+[0-9]>/", $html, $title);
$titles = $title[1];
for ($i = 0; $i < count($titles); $i++) {
	echo "<tr>";
	echo "<td>Заголовки</td>";
	echo "<td>".$titles[$i]."</td>";
	echo "</tr>";
}
echo "</tbody>";
echo "</table>";
?>

</body>
</html>
