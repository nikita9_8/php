<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RequestPage</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<section class="mainSection container">
    <main class="mainSection-form">
        <?php
                if (preg_match("/^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$/", $_POST["first_name"])) {
                echo "Имя введено верно!"."<br>";
                echo $name."<br>";
                }
            else {
                echo "Формат ввода name не верен!"."<br>";
                }
                

                
                
            if (preg_match("/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/", $_POST["password"])) {
                echo "Пароль введен верно!"."<br>";
                echo $password."<br>";
                }
            else {
                echo "Формат ввода password не верен!"."<br>";
                }   
                
                
                
            if (preg_match("/[0-9a-z]+@[a-z]/", $_POST["email"])) {
                echo "Email введен верно!"."<br>";
                echo $mail."<br>";
                }
            else {
                echo "Формат ввода email не верен!"."<br>";
                }
                
        ?>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Логи </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php

                        $arrFromLog = file('login.dat');
                        $outerData = array();

                        foreach ($arrFromLog as $item) {
                            array_push($outerData,$item);
                        }

                        $strLogs = implode("<br>", $outerData);

                        echo "<p class=\"logs text-center\">{$strLogs} </p>";


                        ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
</section>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
