﻿<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lab 3 (Pagination)</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <?php

    $fileArr = file('data.log');



    //Логика сортировки данных
    foreach ($fileArr as $key => $value) {
        $value = explode(" ", $value);
        $array[] = $value;
    }

    if (isset($_POST['sortNumber'])) {

        $sort= array();
        foreach($array as $key => $arr){
            $sort[$key] = $arr[0];
        }
        array_multisort($sort, SORT_ASC, SORT_NUMERIC, $array);

    } elseif (isset($_POST['sortEval'])) {

        $sort= array();
        foreach($array as $key => $arr){
            $sort[$key] = $arr[4];
        }
        array_multisort($sort, SORT_ASC, SORT_NUMERIC, $array);

    } else {

        $sort= array();
        foreach($array as $key => $arr){
            $sort[$key]= $arr[1];
        }
        array_multisort($sort, SORT_ASC, SORT_STRING, $array);

    }

    //Контролы пагинации

    if (isset($_GET['size'])) {
        $count = $_GET['size'];
    } else {
        $count = 10;
    }


    $page = isset($_GET["page"]) ? (int) $_GET["page"] : 0;

    $length = floor(count($fileArr) / $count);

    ?>

    <div class="container">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Фамилия</th>
                <th scope="col">Имя</th>
                <th scope="col">Отчество</th>
                <th scope="col">Бал</th>
            </tr>
            </thead>
            <tbody>
            <? for ($i = $page * $count; $i < ($page + 1) * $count; $i++) { ?>
                <tr>
                    <td><?= $array[$i][0] ?></td>
                    <td><?= $array[$i][1] ?></td>
                    <td><?= $array[$i][2] ?></td>
                    <td><?= $array[$i][3] ?></td>
                    <td><?= $array[$i][4] ?></td>
                </tr>
            <? } ?>
            </tbody>
        </table>
        <form><input type="text" name="size" placeholder="Количество человек"></form>

        <form method="post">

            <input class="btn btn-outline-primary" type="submit" name="sortNumber" value="Сортировать по номеру">
            <input class="btn btn-outline-primary" type="submit" name="sortSurname" value="Сортировать по фамилии">
            <input class="btn btn-outline-primary" type="submit" name="sortEval" value="Сортировать по балу">
        </form>
        <nav class="nav-wrapper">
            <ul class="pagination">
                <?php for ($i = 0; $i <= $length; $i++) { ?>
                    <li class="page-item" ><a class="page-link" href="?page=<?= $i ?>&size=<?= $_GET['size'] ?>"><?= $i + 1 ?></a></li>
                <?php } ?>
            </ul>
        </nav>

    </div>





    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>

