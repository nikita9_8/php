<?php

$string = file('studfile.txt');

foreach ($string as $key => $value) {
    $value = explode(" ", $value);
    $array[] = $value;
}

if (isset($_POST['sortDate'])) {
    $sort=array();
    foreach($array as $key=>$arr){
        $sort[$key]=$arr[4];
    }
    array_multisort($sort, SORT_ASC, SORT_REGULAR, $array);
} elseif (isset($_POST['sortEval'])) {
    $sort=array();
    foreach($array as $key=>$arr){
        $sort[$key]=$arr[5];
    }
    array_multisort($sort, SORT_ASC, SORT_NUMERIC, $array);
} else {
    $sort=array();
    foreach($array as $key=>$arr){
        $sort[$key]=$arr[0];
    }
    array_multisort($sort, SORT_ASC, SORT_STRING, $array);
}

echo '<table border="1px">';
foreach ($array as $key => $value) {
    echo "
		<tr>
			<td>".$value[0]."</td>
			<td>".$value[1]."</td>
			<td>".$value[2]."</td>
			<td>".$value[3]."</td>
			<td>".$value[4]."</td>
			<td>".$value[5]."</td>
		</tr>
	";
}
echo '</table>';

?>

<!DOCTYPE html>
<html>
<head>
    <title>Lab</title>
</head>
<body>
<form method="post">
    <input type="submit" name="sortName" value="Сортувати по имені">
    <input type="submit" name="sortDate" value="Сортувати по даті народження">
    <input type="submit" name="sortEval" value="Сортувати за середнім балом">
</form>
</body>
</html>