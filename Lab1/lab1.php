<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>RequestPage</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<section class="mainSection container">
    <main class="mainSection-form">
        <?php
        /**
         * Created by PhpStorm.
         * User: mikit
         * Date: 06.09.2018
         * Time: 15:25
         */

        $str = "Добрий день,"
            .$_POST["first_name"]. "  "
            .$_POST["last_name"]."!";
        echo "<h1 class=\"display-4 text-center\">{$str}</h1>";
        $arr = array();

        foreach ($_POST["kurs"] as $key => $value ) {
            $arr[$key] .= $value;
        }
        $strArr = implode(", ", $arr);

        echo "<h2 class=\"display-4 text-center\"> Ви вибрали для вивчення курс по {$strArr} </h2>";
        ?>
    </main>
</section>

</body>
</html>
