window.addEventListener('load', (e) => {

    let name = document.getElementById('name');
    let surname = document.getElementById('surname');
    let email = document.getElementById('email');
    let submit = document.getElementById('submit');
    let checkboxses = document.getElementsByName('kurs[]');
    let emptyInputs = document.getElementsByClassName('input-controls');


    submit.addEventListener('click' , (e) => {
        name.nextElementSibling.classList.add('invalid-feedback');
        surname.nextElementSibling.classList.add('invalid-feedback');
        email.nextElementSibling.classList.add('invalid-feedback');
        emptyInputs[0].lastElementChild.classList.add('invalid-feedback');

        let checkedBoxs = false;
        for(let value in checkboxses) {
            if (checkboxses[value].checked) {
                checkedBoxs = true;
            }
        }

        if (name.value.length === 0 || surname.value.length === 0 || email.value.length === 0) {
            if (name.value.length === 0) name.nextElementSibling.classList.remove('invalid-feedback');
            if (surname.value.length === 0) surname.nextElementSibling.classList.remove('invalid-feedback');
            if (email.value.length === 0) email.nextElementSibling.classList.remove('invalid-feedback');
            e.preventDefault();
        }

        if (!checkedBoxs) {
            emptyInputs[0].lastElementChild.classList.remove('invalid-feedback');
            e.preventDefault();
        }
    });


});