<?php

    class Session {

        function __construct() {
            session_start();
        }

        function remove($remove) {
            unset($_SESSION[$remove]);
        }

        function set($key, $value) {
            if ($value != null) {
                $_SESSION[$key] = $value;
            } else {
                unset($_SESSION[$key]);
            }
        }

        function get($get) {
            if (isset($_SESSION[$get])) {
                return $_SESSION[$get];
            }
        }

    }

?>