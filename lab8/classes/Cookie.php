<?php

    class Cookie {
        function set($key, $val) {
            setcookie($key, $val);
        }
        function get($key) {
            return $_COOKIE[$key];
        }
        function del($key) {
            unset($_COOKIE[$key]);
        }
    }

?>