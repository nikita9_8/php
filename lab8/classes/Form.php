<?php
    include_once("SmartForm.php");
    include_once("Log.php");

    class Form {
        protected static $name;
        protected static $anypassword;
        protected $log;
        function __construct()
        {
            $this->log = new Log();
        }

        function writeLog($date) {
            $this->log->write($date);
        }

        function open($open = []) {
            return "<form action='".$open["action"]."' method='".$open["method"]."'>";
        }
        function input($input = []) {
            return "<input type='".$input["type"]."' placeholder='".$input["placeholder"]."' name='".$input["name"]."' value='".self::$name."'>";
        }
        function password($pass = []) {
            return "<input type='password' placeholder='".$pass["placeholder"]."' name='".$pass["name"]."' value='".self::$anypassword."'>";
        }
        function textarea($textarea = []) {
            return "<textarea placeholder='".$textarea["placeholder"]."'>".$textarea["value"]."</textarea>";
        }
        function submit($submit = []) {
            return "<input type='submit' name='submit' value='".$submit["value"]."'>";
        }
        function close() {
            return '</form>';
        }
    }

?>