<?php

    class Log {

        function write($error) {
            $file = 'log.txt';
            $current = file_get_contents($file);
            $current .= $error."\n";
            file_put_contents($file, $current);
        }

        function show() {
            $file = file_get_contents('log.txt', true);
            return $file;
        }

    }

?>