﻿<?php

    include_once("classes/Form.php");
    include_once("classes/SmartForm.php");
    include_once("classes/Log.php");
    include_once("classes/Cookie.php");
    include_once("classes/Session.php");

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style type="text/css">
        form {
            display: flex;
            height: 100vh;
            width: 100vw;
            flex-direction: column;
            max-width: 700px;
            align-items: center;
            justify-content: center;
            margin: 0 auto;
        }

        form > * {
            height: 40px;
            padding-left: 10px;
            width: 100%;
        }
    </style>
</head>
<body>

    <?php

        $form = new Form();
        $smartForm = new SmartForm();

        echo $form->open(['action'=>'index.php', 'method'=>'POST']);
        echo $form->input(['type'=>'text','placeholder'=>'Ваше имя','name'=>'name'])."<br>";
        echo $form->password(['placeholder'=>'Ваш пароль','name'=>'anypassword'])."<br>";
        echo $form->textarea(['placeholder'=>'your important message', 'value'=>'Hello it`s Nik.'])."<br>";
        echo $form->submit(['value'=>'Відправити']);
        echo $form->close();
        $form->writeLog(date('l jS \of F Y h:i:s A'));

    ?>

</body>
</html>